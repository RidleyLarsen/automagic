var _state;
var _drive_state;
var interval;
var drive_data = [];

function getVehicleSpeed(callback) {
  // 010F - vehicle speed km/h
  bluetoothSerial.write('010D\r\n', function () {
    console.log('wrote vss get successfully');
    var available = false;
    bluetoothSerial.available(function (data) {
      bluetoothSerial.read(function (data) {
        console.log(data);
        retval = data.split(' ');
        console.log(retval);
        command = retval[0];
        data = retval[1];
        console.log(data);
        val = parseInt(retval[2], 16);
        if (val === "NaN") {
          getVehicleSpeed(callback);
        } else {
          callback(val);
        }
      }, function () {getVehicleSpeed();});
    }, function () {getVehicleSpeed();});
  }, function () {getVehicleSpeed();});
}


function getVehicleMAF(callback) {
  // 0110 - MAF grams/sec
  bluetoothSerial.write('0110\r\n', function () {
    console.log('wrote maf get successfully');
    var available = false;
    console.log('not available trying again');
    bluetoothSerial.available(function (data) {
      bluetoothSerial.read(function (data) {
        console.log("maf data", data);
        /* ex: ["010C
            41", "0C", "0B", "C0", "
            41", "0C", "0B", "C2", "

            >"]
            there are \r\n characters in there.
        */
        retval = data.split(' ');
        console.log(retval);
        command = retval[0];
        data = retval[1];
        var A = parseInt(retval[2], 16);
        var B = parseInt(retval[3], 16);
        console.log(retval[2], retval[3], A, B, ((A*256)+B) / 100);
        val = ((A*256)+B) / 100;
        if (val === "NaN") {
          getVehicleMAF(callback);
        } else {
          callback(val);
        }
      }, function () {getVehicleMAF();});
    }, function () {getVehicleMAF();});
  }, function () {getVehicleMAF();});
}

function getVehicleMPG(callback) {
  getVehicleMAF(function (maf_data) {
    setTimeout(function () {
      getVehicleSpeed(function (speed_data) {
        callback((710.7 * speed_data) / maf_data);
      });
    }, 500);
  });
}

function getSettingUseGPS () {
  if (window.localStorage['use_gps'] === "true") {
    return true;
  } else {
    return false;
  }
}

function getSettingUseBluetooth () {
  if (window.localStorage['use_bluetooth'] === "true") {
    return true;
  } else {
    return false;
  }
}

function setDriveList(data) {
  window.localStorage['data'] = JSON.stringify(data);
}

function getDriveList() {
  if (window.localStorage['data']) {
    data = JSON.parse(window.localStorage['data']);
    data.reverse();
    return data;
  } else {
    return [];
  }
}

$(function () {
  $(document).on('click', '.navbar-nav li', function () {
    $('.navbar-nav li').removeClass('active');
    $(this).addClass('active');
    app.changeScreen($(this).data('screen'));
  });
  app.changeScreen("drive");
  $(document).on('click', '.btn-start-drive', function () {
    app.startDrive();
    $(this).blur();
  });
  $(document).on('click', '.btn-stop-drive', function () {
    app.stopDrive();
    $(this).blur();
  });
  $(document).on('touchdown', $('.drive-detail'), function (e) {
    longpress = true;
    setTimeout(function () {
      if (longpress) {
        graph_data.data.reverse();
        graph_data.data.splice($(this).attr('id'), 1);
        setDriveList(graph_data.data);
      }
    }, 850)
  });
  $(document).on('click', '.drive-detail', function (e) {app.getDriveDetail($(this).attr("id"));});
  $('body').append('<script src="https://maps.googleapis.com/maps/api/js?key=' + google_maps_api_key + "&libraries=geometry" + '"></script>');
});

var app = {
    macAddress: "00:1D:AE:40:0E:E3",
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    getDriveData: function () {
      // TODO: real data.
      return {'driving': _drive_state,};
    },
    getGraphData: function () {
      // TODO: real data.
      return {'data': getDriveList()};
    },
    getSettingsData: function () {
      // TODO: real data.
      return {
        'use_gps': getSettingUseGPS(),
        'use_bluetooth': getSettingUseBluetooth(),
      };
    },
    getDriveDetail: function (drive_id) {
      _state = "detail";
      data = getDriveList();
      var coordinates = data[drive_id].data;
      var sticky_height = $('.sticky-bottom').height() || 0;
      template = Handlebars.compile($('#template-drive-detail').html());
      used_mpg = false;
      for (var i = 0; i < data[drive_id].data.length; i++) {
        if (data[drive_id].data[i].mpg) {
          used_mpg = true;
        }
      }
      html = template({'data': data[drive_id], 'mpg': used_mpg});
      $('.container').css('height', $(window).height() - sticky_height - $('.navbar').height());
      $('#screen').html(html);
      $('#map').css({
        'width': '100%',
        'height': '50vh',
      });
      // try {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 3,
          center: {lat: 0, lng: 0},
          mapTypeId: google.maps.MapTypeId.ROADMAP,
        });

        // var path = new google.maps.Polyline({path: data[drive_id].data, geodesic: true, strokeColor: "#00CC00", strokeOpacity: 1.0, strokeWeight: 2});
        var path_data = data[drive_id].data;
        for (var i = 0; i < path_data.length-1; i++) {
          oldrange = (path_data.length - 1);
          newrange = (256 - 0);
          red = Number(((i * newrange) / oldrange).toFixed(0));
          red = red.toString(16);
          // red = ((i / (255 / path_data.length)) * 255).toString(16);
          if (red.length == 1) {
            red = "0" + red;
          }
          color = "#" + red + "0000";
          var PathStyle = new google.maps.Polyline({
            path: [path_data[i], path_data[i+1]],
            strokeColor: color.toString(16),
            strokeOpacity: 1.0,
            strokeWeight: 2,
            map: map
          });
        }
        // path.setMap(map);

        var bounds = new google.maps.LatLngBounds();
        for (var n = 0; n < coordinates.length; n++) {
            bounds.extend(new google.maps.LatLng(coordinates[n].lat, coordinates[n].lng));
        }
        map.fitBounds(bounds);

        google.load('visualization', '1.0', {'packages':['corechart'], callback: function () {
          var data = new google.visualization.DataTable();
          data.addColumn('number', 'Point');
          data.addColumn('number', 'MPG');
          mpgs = [];
          for (var i = 0; i < path_data.length; i++) {
            if (path_data[i].mpg) {
              mpg = path_data[i].mpg;
              if (mpg > 1000) {
                mpg = mpg / 1000;
              }
              if (mpg > 100) {
                mpg = mpg / 100;
              }
              mpgs.push([i, mpg]);
            }
          }
          if (mpgs.length > 0) {
            data.addRows(mpgs);
            var chart = new google.visualization.LineChart(document.getElementById('line-chart'));
            chart.draw(data, {title: "Drive Miles per Gallon"});
          }
        }});

      // } catch (e) {
      //   alert("Data has been corrupted. Could not load the map API. Proceeding to delete this record. Sorry!");
      //   data.splice(drive_id, 1);
      // }
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
      app.receivedEvent('deviceready');
      console.log('001DAE400EE3');
      document.addEventListener("backbutton", function(e){
        console.log("e:", e, "_state:", _state);
        if(_state !== "detail"){
          e.preventDefault();
          navigator.app.exitApp();
        } else {
          app.changeScreen("graphs");
        }
      }, false);
    },
    manageConnection: function () {
      var connect = function () {
        console.log("Attempting to connect.");
        bluetoothSerial.connect(
          app.macAddress,
          app.openPort,
          app.showError
        );
      };
      var disconnect = function () {
        console.log("Attempting to disconnect.");
        bluetoothSerial.disconnect(
          app.closePort,
          app.showError
        );
      };
      bluetoothSerial.isConnected(disconnect, connect);
    },
    openPort: function () {
      console.log("connected to: " + app.macAddress);
      bluetoothSerial.subscribe('\n', function (data) {
        console.log("Data from open port:", data);
      });
    },
    closePort: function () {
      // if you get a good Bluetooth serial connection:
        console.log("Disconnected from: " + app.macAddress);
        // unsubscribe from listening:
        bluetoothSerial.unsubscribe(
          function (data) {
            console.log("Closed Port:", data);
          },
          app.showError
        );
    },

    changeScreen: function (screen) {
      _state = screen;
      template = Handlebars.compile($('#template-' + screen + '').html());
      if (screen == "drive") {
        html = template(app.getDriveData());
      } else if (screen == "graphs") {
        graph_data = app.getGraphData();
        html = template(graph_data);
      } else if (screen == "settings") {
        html = template(app.getSettingsData());
      }
      $('#screen').html(html);
      $('input[type="checkbox"]').bootstrapSwitch({
        onSwitchChange: function(event, state) {
          if ($(event.target).attr('type') === "checkbox") {
            window.localStorage[$(event.target).data('setting')] = $(this).is(":checked");
          }
        }});
      var sticky_height = $('.sticky-bottom').height() || 0;
      $('.container').css('height', $(window).height() - sticky_height - $('.navbar').height());
    },

    showError: function(error) {
        console.log(error);
    },

    startDrive: function () {
      drive_data = [];
      use_bt = getSettingUseBluetooth();
      if (use_bt) {
        app.manageConnection();
      }
      _drive_state = true;
      var geoSuccess = function (data) {
        console.log(data);
        if (data.coords.accuracy < 50) {
          if (use_bt) {
            getVehicleMPG(function (mpg_data) {
              drive_data.push({
                'lng': data.coords.longitude,
                'lat': data.coords.latitude,
                'mpg': mpg_data,
              });
            });
          } else {
            drive_data.push({
              'lng': data.coords.longitude,
              'lat': data.coords.latitude,
              'mpg': 0,
            });
          }
        }
      };
      var geoFailure = function (data) {
        console.log(data);
      };
      interval = setInterval(function () {
        navigator.geolocation.getCurrentPosition(geoSuccess, geoFailure, {maximumAge: 500, timeout: 3000, enableHighAccuracy: getSettingUseGPS()});
      }, 3000);
      $('.btn-start-drive').addClass('btn-stop-drive btn-danger').removeClass('btn-start-drive btn-success');
      $('.btn-stop-drive').text('Stop Drive');
    },
    stopDrive: function () {
      _drive_state = false;
      clearInterval(interval);
      if (drive_data.length > 0) {
        data = getDriveList();
        data.reverse();
        m = new moment();
        drive = {
          'date': m.toISOString(),
          'start_date': m.format("MMM Do[,] YYYY"),
          'start_time': m.format("h:mm a"),
          'data': drive_data,
        };
        data.push(drive);
        setDriveList(data);
      }
      $('.btn-stop-drive').addClass('btn-start-drive btn-success').removeClass('btn-stop-drive btn-danger');
      $('.btn-start-drive').text('Start Drive');
    }
};

app.initialize();
/*

OBD PIDs:

0B - 1 byte - Manifold Air Pressure 0-765 kPa
0F - 1 byte - Intake Air Temp (-40 - 215)

0D - 1 byte - Vehicle speed 0-255 km/h
10 - 2 bytes - MAF air flow rate (0 - 655.35 g/sec)


MPG calculation

Engine Mass Airflow = RPM * (Manifold Air Pressure / Absolute Temperature)
Fuel Mass = Engine Mass Airflow / (Stoichiometric Ratio * Lambda (commanded air/fuel ratio))


MPG = (14.7 * 6.17 * 4.54 * VSS * 0.621371) / (3600 * MAF / 100)
= 710.7 * VSS / MAF


MPG - miles per gallon
14.7 grams of air to 1 gram of gasoline - ideal air/fuel ratio
6.17 pounds per gallon - density of gasoline
4.54 grams per pound - conversion
VSS - vehicle speed in kilometers per hour
0.621371 miles per hour/kilometers per hour - conversion
3600 seconds per hour - conversion
MAF - mass air flow rate in 100 grams per second
100 - to correct MAF to give grams per second
*/
/*
read 010D
41 0D 00
41 0D 00

>
*/
